Installation
############

PyPI
----

There's a ready-made *rectify* package on PyPI which can be installed in the
following way:

.. code-block:: sh

   $ python3 -m venv rectify.env
   $ source rectify.env/bin/activate
   $ pip install rectify


Source
------

*rectify* can also be installed directly from source.

First, get the latest version of the source code from GitLab:

.. code-block:: sh

   $ git clone git@gitlab.com:jenx/rectify.git

Create and activate a virtual environment to keep *rectify* and its
dependencies separate from system packages:

.. code-block:: sh

   $ cd rectify
   $ python3 -m venv rectify.env
   $ source rectify.env/bin/activate

Install *rectify*:

.. code-block:: sh

   $ python setup.py install

You are good to go! You can test whether the setup works by importing
*rectify* in the Python shell...

.. code-block:: python

   >>> import rectify
   >>> # no ImportError == win

...or by using the ``rectify`` command.

.. code-block:: sh

   $ rectify --help


Troubleshooting
---------------

* Pillow, a dependency of *rectify*, needs a couple of external libraries (like
  ``libjpeg-dev`` and ``zlib1g-dev``) to build. See `the Pillow installation
  page <https://pillow.readthedocs.io/en/latest/installation.html>`_ for more
  information.
