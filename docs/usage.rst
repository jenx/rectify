Usage
#####

*rectify* provides two interfaces for image generation: It can either be
imported as a Python package or invoked directly from the command line.


As a Python Package
-------------------

:func:`rectify.image.generate` is the partner for all your colorful bar
generating needs.

.. code-block:: python

   >>> from rectify.image import generate, save

   >>> # Generate an image with arbitrary colors
   >>> picture = generate()

   >>> # Generate a 400x200 px image with a neon background and grayscale
   >>> # bars
   >>> picture = generate(width=400, height=200, background='neon',
   ...                    colors='grayscale')

   >>> # View and save the generated image
   >>> picture.show()
   >>> save(picture, '~/where/to/save/bars.png')

For the full list of customization options, please refer to
:func:`rectify.image.generate`.


Command Line Interface
----------------------

*rectify* image generation can also be invoked via the command line. The
resulting picture can either be shown (``-s | --show``) or saved to a file
(``-o | --output``).

The background color and the colors of the bars generated are customizable
via palettes. For the purposes of the command line interface, a **color** is:

- ``(r, g, b)`` with ``0 <= r, g, b <= 255``
- anything interpretable as a color by `Pillow's ImageColor
  <http://pillow.readthedocs.io/en/latest/reference/ImageColor.html>`_

A **palette** is:

- a color
- a list of colors
- a predefined palette preset (see :mod:`rectify.palette`)

So, for instance, ``(0, 0, 0)`` is a palette containing only black, while
``[(255, 0, 0), (0, 255, 0), (0, 0, 255)]`` is a palette consisting of pure
red, green and blue, and ``pastel`` is a palette of light colors with low to
medium saturation.

.. code-block:: sh

   $ # Generate and show an image with random background and bars
   $ rectify --show

   $ # Generate a 400x200 image with grayscale bars on a neon color
   $ rectify --width 400 --height 200 --background neon --colors grayscale

   $ # Generate, show and save
   $ rectify --show --output 'picture.png'

For the full list of supported options, please run ``rectify --help``.
