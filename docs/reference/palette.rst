Palette
#######

.. automodule:: rectify.palette


Presets
-------

*rectify* provides a number of predefined palettes. These can be either passed
to functions expecting a palette directly as callables or by their name as
string.


Basic
~~~~~

.. autofunction:: rectify.palette.random

.. autofunction:: rectify.palette.black

.. autofunction:: rectify.palette.white

.. autofunction:: rectify.palette.grayscale

.. autofunction:: rectify.palette.dark

.. autofunction:: rectify.palette.light

.. autofunction:: rectify.palette.pastel

.. autofunction:: rectify.palette.neon

.. autofunction:: rectify.palette.warm

.. autofunction:: rectify.palette.cold


Additional
~~~~~~~~~~

.. autofunction:: rectify.palette.eight_bit


Functions
---------

.. autofunction:: rectify.palette.pick_color

.. autofunction:: rectify.palette.rgb
