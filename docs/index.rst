rectify
=======

``rectify`` is a tiny Python package whose sole purpose of life is generating
randomized images with colorful bars on a single-color background. Making,
displaying and saving a picture like the one in the sidebar of this page is as
simple as:

.. code-block:: python

   from rectify.image import generate, save

   picture = generate(220, 30, background=(30, 30, 30), colors='neon')
   picture.show()
   save(picture, 'picture.png')

Or from the command line:

.. code-block:: sh

   $ rectify -x 220 -y 30 -b '(30,30,30)' -c neon -o 'picture.png'


Documentation
#############

.. toctree::
   :maxdepth: 2

   installation
   usage
   reference/image
   reference/palette


Indices and Tables
##################

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
