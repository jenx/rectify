# To Do


## Implementation

- [x] Implement basic generator
- [x] Implement CLI
- [ ] Add customization options
    - [x] Background color
    - [x] Stripe colors
    - [x] Stripe amount
    - [ ] Stripe thickness
    - [ ] Stripe positions
    - [ ] Stripe orientation
    - [ ] Stripe color relationships (analogous, triadic, etc.)
- [ ] Add palettes
    - [x] Basic set
    - [ ] Extra presets
- [ ] Add overlays
    - [ ] Implement overlays
    - [ ] Provide overlay presets
- [x] Add saving functionality
- [x] Implement tests
    - [x] Basic generator
    - [x] CLI
    - [ ] Customization
    - [x] Palettes
    - [ ] Filters
- [x] Add documentation
    - [x] Function docstrings
    - [ ] Docstring examples
    - [x] Module docstrings
    - [x] Tutorial page
- [ ] Investigate Python support
    - [x] Test with older Python 3 versions
    - [ ] Consider Python 2 support


## Misc

- [x] Write a proper README.md
    - [x] Include an example
    - [x] Include a link to the docs
- [x] Update setup.py
    - [x] Investigate more classifiers
- [x] Generate documentation
    - [x] Host somewhere accessible
- [x] Automated test pipeline
- [x] Release notes for future releases
