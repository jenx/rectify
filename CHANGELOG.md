# v2.2
_November 27, 2018_

- Explicitly support Python 3.7+
- Remove Sphinx as dependency
- Remove requirements.txt and add minimum dependency versions to setup.py


# v2.1
_November 27, 2018_

- Release superseded by 2.2


# v2.0
_June 24, 2018_

- Rename "bars" to "colors" in both the command line interface and in the main
  generate function
- Only warn about unsufficient space for target number of bars if the number
  has been specified by the user (and not randomly generated internally)
- Fix minor mistake in README
- Minor rewording of docs


# v1.0.1
_June 17, 2018_

- Fix Markdown display on PyPI
- Bump the development status classifier to Beta


# v1.0
_June 17, 2018_

rectify 1.0 is out! Please refer to the documentation for the base set of
features and how-to's.
